$(function() {  
    //Here is the way of executing a javascript function on form submit without reload the html page. return false; is the key to prevent the from to reolad the html page.
    $(document).on('submit', '#form', function() {
        return false;
    });       
    // ------------------------
    $("#car").change(function(){
        // MERCEDES BENZ
        if( $("#Mercedes-Benz").is(":selected") ){
            $('#2008-2011').prop('disabled', false)
            $('#2012-2015').prop('disabled', false)
            $('#2016-2019').prop('disabled', true)
            $('#25000-50000').prop('disabled', false)
            $('#5000-20000').prop('disabled', false)
            $('#100k').prop('disabled', true)
            $("#year").change(function(){
            if( $("#2008-2011").is(":selected") ){
                $('#5000-20000').prop('disabled', false)
                $('#25000-50000').prop('disabled', true)
                $('#100k').prop('disabled', true)
            }
            else{
                $('#25000-50000').prop('disabled', false)
                $('#5000-20000').prop('disabled', true)
                $('#100k').prop('disabled', true)
            }
            });
        }
        ///////////////////////////////////////////////////////////////////////////////

        // VOLKSWAGEN
        if( $("#Volkswagen").is(":selected") ){
            
            $('#2008-2011').prop('disabled', true)
            $('#2012-2015').prop('disabled', true)
            $('#2016-2019').prop('disabled', false)
            $("#year").change(function(){
                $('#5000-20000').prop('disabled', true)
                $('#25000-50000').prop('disabled', false)
                $('#100k').prop('disabled', true)            
            });
        }
        ///////////////////////////////////////////////////////////////////////////////

        // BMW
        if( $("#BMW").is(":selected") ){
            
            $('#2008-2011').prop('disabled', true)
            $('#2012-2015').prop('disabled', false)
            $('#2016-2019').prop('disabled', false)

            $('#5000-20000').prop('disabled', false)
            $('#25000-50000').prop('disabled', true)
            $('#100k').prop('disabled', false)
            $("#year").change(function(){

                if( $("#2012-2015").is(":selected") ){
                    $('#5000-20000').prop('disabled', false)
                    $('#25000-50000').prop('disabled', true)
                    $('#100k').prop('disabled', true)
                }
                else{
                    $('#5000-20000').prop('disabled', true)
                    $('#25000-50000').prop('disabled', true)
                    $('#100k').prop('disabled', false)
                }          
            });
        }
        ///////////////////////////////////////////////////////////////////////////////

        // Audi
        if( $("#Audi").is(":selected") ){
            
            $('#2008-2011').prop('disabled', true)
            $('#2012-2015').prop('disabled', true)
            $('#2016-2019').prop('disabled', false)

            $('#5000-20000').prop('disabled', true)
            $('#25000-50000').prop('disabled', false)
            $('#100k').prop('disabled', false)
        }
        ///////////////////////////////////////////////////////////////////////////////
    });
     
    //Search
    $("#search").click(function() {
        
        if ($("#Mercedes-Benz").is(":selected") && $("#2008-2011").is(":selected") && $("#5000-20000").is(":selected")) {
        // Clean if there is an article above
        $("article").first().css("display", "none");
        $("#filter").css("display", "none");
        $("#filter-button").css("display", "block");
        //---------------------
        // Display the article
        $("#MB_8-11Y_5-20K").css("display","block");
        } 
        //---------------------
        else if($("#Mercedes-Benz").is(":selected") && $("#2012-2015").is(":selected") && $("#25000-50000").is(":selected")){
            // Clean if there is an article above
            $("article").first().css("display", "none")
            $("#filter").css("display", "none");
            $("#filter-button").css("display", "block");    
            //---------------------
            // Display the article
            $("#MB_12-15Y_25-50K").css("display","block")
        } 
        else if($("#Volkswagen").is(":selected") && $("#2016-2019").is(":selected") && $("#25000-50000").is(":selected")){
            // Clean if there is an article above
            $("article").first().css("display", "none")
            $("#filter").css("display", "none");
            $("#filter-button").css("display", "block");    
            //---------------------
            // Display the article
            $("#Volkswagen-1").css("display","block")
        }
        else if($("#BMW").is(":selected") && $("#2012-2015").is(":selected") && $("#5000-20000").is(":selected")){
            // Clean if there is an article above
            $("article").first().css("display", "none") 
            $("#filter").css("display", "none");
            $("#filter-button").css("display", "block");   
            //---------------------
            // Display the article
            $("#bmw-1").css("display","block")
        }
        else if($("#BMW").is(":selected") && $("#2016-2019").is(":selected") && $("#100k").is(":selected")){
            // Clean if there is an article above
            $("article").first().css("display", "none") 
            $("#filter").css("display", "none");
            $("#filter-button").css("display", "block");   
            //---------------------
            // Display the article
            $("#bmw-2").css("display","block")
        } 
        else if($("#Audi").is(":selected") && $("#2016-2019").is(":selected") && $("#25000-50000").is(":selected")){
            // Clean if there is an article above
            $("article").first().css("display", "none") 
            $("#filter").css("display", "none");
            $("#filter-button").css("display", "block");   
            //---------------------
            // Display the article
            $("#audi-1").css("display","block")
        } 
        else if($("#Audi").is(":selected") && $("#2016-2019").is(":selected") && $("#100k").is(":selected")){
            // Clean if there is an article above
            $("article").first().css("display", "none") 
            $("#filter").css("display", "none");
            $("#filter-button").css("display", "block");   
            //---------------------
            // Display the article
            $("#audi-2").css("display","block")
        } 
        else if($("#sm").is(":selected")){
            swal('Select make')
        }
        else if($("#y").is(":selected")){
            swal('Select year')
        }
        else if($("#p").is(":selected")){
            swal('Select price')
        }
        else{
            $("#filter").show()
            // Display the alert if there is no content found
            swal("No Content Found");
            //---------------------
        }
        window.scrollBy(0,80)
        
    });
    // ------------------------
    $("#filter-button").click(function(){
        $("#filter-button").hide();
        $("article").css("display", "none");
        $("#filter").css("display", "block");
        $("#car").val("select-make").change();
        $("#year").val("year").change();
        $("#price").val("price").change();
    });
    $("#form").submit(function() {
        if ( $("#f1").val().length != 0 && $("#f2").val().length != 0 && $("#f3").val().length != 0) 
        {
            swal("Thank you!", "Your message has been sent successfully", "success").then(function(){ 
            location.reload();
            }
            );
        } 
    });  
    $("#to-top").click(function(){
        window.scrollTo(0,0)
    });          
});
